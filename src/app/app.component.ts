import {Component} from '@angular/core';

@Component({
  selector: 'app-root', templateUrl: './app.component.html', styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'nfc-app';
  msg: DataView| string | undefined = '';
  test = 'test';

  write() {
    if ("NDEFReader" in window) {

      const ndef = new NDEFReader();
      ndef.write("Guillaume").then(() => {
        this.msg = 'Ok';
      }).catch((error: any) => {
        this.msg = error;
      });
    }
  }

  read() {
    if ("NDEFReader" in window) {

      const ndef = new NDEFReader();
      ndef.scan()
        .then(() => {
          ndef.onreadingerror = (event) => {
            this.msg = "Error! Cannot read data from the NFC tag. Try a different one?";
          };
          ndef.onreading = (event) => {
            const text = new TextDecoder
            // @ts-ignore
            this.msg= text.decode(event.message.records.at(0).data.buffer)
            this.test = event.serialNumber;
          };
        })
        .catch((error) => {
          this.msg =`Error! Scan failed to start: ${error}.`;
        });
    }
  }
}
